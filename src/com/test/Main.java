package com.test;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
   public static void main(String[] args) {
      do {
         Scanner input = new Scanner(System.in);
         try {
            System.out.printf("\nEnter numerator: ");
            int num = input.nextInt();
            System.out.printf("\nEnter denominator: ");
            int denom = input.nextInt();
            int quo = num / denom;
            System.out.printf("\nResult = " + quo);
         }catch (InputMismatchException ex1){
            System.err.println("Sorry, input does not match. Try again.");
            input.nextLine();
            System.err.println("Enter only numbers.");
         }catch(ArithmeticException ex2){
            System.err.println("Please enter a non-zero denominator. Try again.");
            ex2.printStackTrace();
         }finally {
            System.out.printf("\nDo you want to continue (y/n):");
            String choice = input.next();
            if (!choice.trim().equals("y"))
               break;
         }
      }while(true);
   }
}
package com.anonymousClass;

public class Duck implements Flyable, Swimable{
    @Override
    public void swim() {
        System.out.println("Utka plavaet");
    }

    @Override
    public void fly() {
        System.out.println("Utka letaet");
    }
}

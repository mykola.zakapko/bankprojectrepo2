package com.company.validator;

public class AccountNumberValidator implements Validator<Long> {
    @Override
    public boolean isValid(Long inputData) {
        return inputData < 0;
    }
}

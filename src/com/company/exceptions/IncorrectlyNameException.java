package com.company.exceptions;

public class IncorrectlyNameException extends Exception {


    public IncorrectlyNameException(String userName) {
        super("The name is entered incorrectly: " + userName);
    }
}

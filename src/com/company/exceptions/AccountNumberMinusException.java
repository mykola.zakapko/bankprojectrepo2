package com.company.exceptions;

public class AccountNumberMinusException extends Exception {

    public AccountNumberMinusException(long num) {
            super("You entered an incorrect account number"+ num);
    }
}

package com.company.dataprovider;

import com.company.enums.CurrencyType;
import com.company.exceptions.AccountNumberMinusException;
import com.company.exceptions.BalanceMinusException;
import com.company.exceptions.IncorrectlyNameException;
import com.company.exceptions.InvalidCurrencyException;
import com.company.implementation.BankAccount;
import com.company.validator.AccountNumberValidator;
import com.company.validator.HolderNameValidator;
import com.company.validator.Validator;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleDataProvider implements DataProvider {

    private static Scanner scanner = new Scanner(System.in);

    @Override
    public String getHolderName() {
        HolderNameValidator holdernameValidator = new HolderNameValidator();
        String holderName = "";
        System.out.println("Enter account holder name : ");
        try {
            holderName = scanner.nextLine();
            if (!holdernameValidator.isValid(holderName)) {
                throw new IncorrectlyNameException(holderName);
            }
        } catch (InputMismatchException e) {
            System.err.println("You should provide only String value");
            getHolderName();
        } catch (IncorrectlyNameException e) {
            System.err.println(e.getMessage());
            getHolderName();
        }
        return holderName;
    }

    @Override
    public long getAccountNumber() {
        AccountNumberValidator numberValidator = new AccountNumberValidator();
        long accountNumber = 0;
        System.out.println("Please, enter an account number: ");
        try {
            accountNumber = scanner.nextLong();
            if (!numberValidator.isValid(accountNumber)) {
                throw new AccountNumberMinusException(accountNumber);
            }
        } catch (InputMismatchException e) {
            System.err.println("You should provide only Number value");
            getHolderName();
        } catch (AccountNumberMinusException e) {
            System.err.println(e);
            getAccountNumber();
        }
        return accountNumber;
    }

    @Override
    public CurrencyType getCurrency() {
        int currencyType;
        System.out.println("Enter account currency: '\n' 1- dollar '\n' 2- euro '\n' 3-uah ");
        currencyType = scanner.nextInt();
        try {
            switch (currencyType) {
                case 1:
                    return CurrencyType.DOLLAR;
                case 2:
                    return CurrencyType.EURO;
                case 3:
                    return CurrencyType.UAH;
                default:
                    throw new InvalidCurrencyException();
            }
        } catch (InvalidCurrencyException e) {
            System.err.println(e.getMessage());
            getCurrency();
        }
//        System.out.println("Account opened successfully \n");
//        return null;
        return CurrencyType.UAH;
    }

    @Override
    public void withdrawalMoney(BankAccount bankAccount) {
        double sum;
        System.out.println("Enter the amount you wish to withdraw: ");
        try {
            sum = scanner.nextDouble();
            bankAccount.getMoney(sum);
        } catch (InputMismatchException e) {
            System.err.println("You should provide only Number value");
            withdrawalMoney(bankAccount);
        } catch (BalanceMinusException e) {
            e.printStackTrace();
            withdrawalMoney(bankAccount);
        }
    }
    @Override
    public double getBalance() {
        double balance = 0;
        System.out.println("Enter the amount you want to deposit: ");
        try {

            balance = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.err.println("You should provide only Number value");
            getBalance();
        }
        return balance;
    }

}
